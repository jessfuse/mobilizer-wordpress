<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

$defaults = array(
	'WP_DEBUG' => false,
	'DB_NAME' => 'getMobilizer',
	'DB_HOST' => 'localhost',
	'DB_USER' => 'root',
	'DB_PASSWORD' => 'root'
	// 'WP_DEBUG' => true,
	// 'DB_NAME' => 'mobilizerwpdb',
	// 'DB_USER' => 'mwpuser',
	// 'DB_PASSWORD' => 'mobilizer2.0',
	// 'DB_HOST' => 'localhost'
);


// include config
$config = include(dirname(__FILE__) . '/config/dev.php');

foreach($defaults as $key => $value)
	if(!defined($key))
		define($key, isset($config[$key]) ? $config[$key] : $value);


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N<CA(Q1_&5T/-,371f(674Ga?JlsuY0(}nql7MOS{P]tZzEp7:y3e_k#c[jClR*L');
define('SECURE_AUTH_KEY',  'XxOXY0]xS`I03zN[EBq}A+6/tm>46FeP*das=$IaE9_|t7`J2]8?f0}EZ*~T|l{~');
define('LOGGED_IN_KEY',    'Zf|9Z<LJHcBuv2.J+4jVL3qQ:%LMNO-uX#R}SsZ9:sP#, Q!,=T21$17M:6P&rn4');
define('NONCE_KEY',        '%CoiU-2{-hF:OSS`leKpVy%sA89hU^=w0*%Jg|4|Yma&Wn_;s(2`P-%SRB|Q|LKL');
define('AUTH_SALT',        'Yob0xx=-l.c V*2XBX1>|dE|5~M1lcwH!+thsS-QbsQh=hLg-%KfL&fzt3P6hp(?');
define('SECURE_AUTH_SALT', '<%`9fhnognJ}0Q;2, ZE0K1s`=%iyWi1#f^vc5E9/{h3%WAO (Zm4I2c|X(`E.~`');
define('LOGGED_IN_SALT',   'p5RRraZT$UPOH>x$r2Q#m2[)4D[BSZ|&l(?+}-@j*v{@37:>ME.>%Ok|z|vGRKS;');
define('NONCE_SALT',       '<AF/~t |l1OS#N/Fx.=8Qub7sY[2-9QYqIBM9<|ls6@7-*J|k8i/c3*:@2uulan?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
// define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
