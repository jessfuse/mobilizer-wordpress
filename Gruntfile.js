'use strict';

module.exports = function (grunt) {

    var base = 'public/wp-content/themes/mobilizer';

    // Load grunt tasks automatically
    require('load-grunt-config')(grunt, {
        data: {
            paths: {
                base: base,
                src: base + '/client/src',
                dist: base + '/client/dist',
            }  
        }
    });
};
