module.exports = {
    dev: {
      expand: true,
      cwd: '<%= paths.dist %>/css',
      src: [ '*.css', '!*.min.css' ],
      dest: '<%= paths.dist %>/css',
      ext: '.min.css'
    }
};
