module.exports = {
    gruntfile: {
        files: ['Gruntfile.js']
    },

    classes: {
        files: [ '<%= paths.base %>/classes/**/*.php' ],
        tasks: ['composer:dump-autoload', 'notify:composer']
    },

    sass: {
        files: [ '<%= paths.src %>/scss/**/*.scss' ],
        tasks: ['sass', 'autoprefixer', 'csso', 'notify:watch'],
        options: {
            livereload: false
        }
    },

    css: {
        files: [ 
            '<%= paths.dist %>/css/**/*.css',
            '!<%= paths.dist %>/css/**/*.min.css'
        ],
        options: {
            livereload: true
        }
    },

    images: {
        files: [ '<%= paths.src %>/images/**/*.{gif,jpeg,jpg}' ],
        tasks: [ 'newer:imagemin'],
        options: {
            livereload: false
        }
    },

    png: {
        files: [ '<%= paths.src %>/images/**/*.png' ],
        tasks: [ 'newer:pngmin'],
        options: {
            livereload: false
        }
    },

    optimizedImages: {
        files: [ '<%= paths.dist %>/images/**/*.{gif,jpeg,jpg,png}' ],
        options: {
            livereload: true
        }
    },

    scriptLibs: {
        files: [  '<%= paths.src %>/bower_components/**/*.js','<%= paths.src %>/scripts/include/*.js'],
        tasks: [ 'uglify:libs', 'notify:watch' ],
        options: {
            livereload: true
        }
    },

    scripts: {
        files: [ '<%= paths.src %>/scripts/**/*.js' ],
        tasks: [ 'newer:uglify:apps', 'notify:watch' ],
        options: {
            livereload: true
        }
    },

    php : {
        files: ['<%= paths.base %>/**/*.php'],
        options: {
            livereload: true
        }
    }
    
}