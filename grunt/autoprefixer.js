module.exports = {
    dev: {
        expand: true,
        flatten: true,
        src: [
            '<%= paths.dist %>/css/*.css',
            '!<%= paths.dist %>/css/*.min.css'
        ],
        dest: '<%= paths.dist %>/css'
    }
};
