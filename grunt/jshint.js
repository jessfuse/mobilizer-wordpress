module.exports = {
    dist: [
        'Gruntfile.js', 
        '<%= paths.src %>/scripts/*.js',
        '<%= paths.src %>/scripts/include/*.js'
    ],
    options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
    }
};
