module.exports = {
	watch: {
		options: {
	        title: 'Build complete',
	        message: 'Refreshing'
	    }
	},
	composer: {
		options: {
	        title: 'Task Complete',
	        message: 'Composer Updated'
	    }
	}
};
