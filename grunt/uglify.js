module.exports = {
    options: {
        // mangle: false,
        // beautify: true
    },
    libs: {
        files: {
             '<%= paths.dist %>/scripts/lib.min.js': [
                '<%= paths.src %>/bower_components/jquery-1.11.1/index.js',
                '<%= paths.src %>/bower_components/fastclick/lib/fastclick.js',
                '<%= paths.src %>/bower_components/skrollr/dist/skrollr.min.js',
                '<%= paths.src %>/bower_components/skrollr-stylesheets/dist/skrollr.stylesheets.min.js',
                '<%= paths.src %>/bower_components/momentjs/moment.js',
                '<%= paths.src %>/bower_components/moment.twitter/moment-twitter.js',
                '<%= paths.src %>/scripts/include/*.js',
             ],
             '<%= paths.dist %>/scripts/polyfill.min.js': [
                '<%= paths.src %>/bower_components/jquery-1.11.1/index.js',
                '<%= paths.src %>/bower_components/html5shiv/dist/html5shiv.js',
                '<%= paths.src %>/bower_components/selectivizr/selectivizr.js',
                '<%= paths.src %>/bower_components/respond/dest/respond.src.js'
             ],
             '<%= paths.dist %>/scripts/modernizr.min.js': [
                '<%= paths.src %>/scripts/vendor/modernizr.js'
             ],
             '<%= paths.dist %>/scripts/raphael.min.js': [
                '<%= paths.src %>/scripts/vendor/raphael.js'
             ],
             '<%= paths.dist %>/scripts/jquery.bxslider.min.js': [
                '<%= paths.src %>/bower_components/bxslider-4/jquery.bxslider.min.js'
             ]
        }
    },
    apps: {
        files: [
            {
                expand: true,
                cwd: '<%= paths.src %>/scripts',
                src: '*.js',
                dest: '<%= paths.dist %>/scripts',
                ext: '.min.js'
            }
        ]
    },
};
