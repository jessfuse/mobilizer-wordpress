module.exports = {
  imagemin: [
  	'imagemin',
  	'pngmin'
  ],
  uglify: [
  	'uglify:libs',
  	'uglify:apps'
  ]
};
