<!doctype html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/client/dist/css/main.min.css">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script type="text/javascript" src="//use.typekit.net/yng0bry.js"></script>
	<script type="text/javascript">try {
			Typekit.load();
		} catch (e) {
		}</script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/specialUrl.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/configUrl.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.0.3.min.js"></script>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/client/dist/scripts/polyfill.min.js"></script>
	<![endif]-->
</head>
<body <?php body_class(); ?>>

<div class="top-bar">
	<a href="/mobilizer"><div class="logo"></div></a>
	<nav>
		<a href="" class="selected">Home</a>
		<a href="/on-screen">On Screen</a>
		<a href="/on-the-metal">On the Metal</a>
	</nav>
	<div class="url-secondary">
		<div class="app-toggle">
			<div class="live-preview-button">On Screen</div>
			<div class="pixel-perfect-button">On the Metal</div>
		</div>
		<input class="url-input" id="enteredUrlTop" placeholder="www."/>
		<div class="go-button">Go</div>
	</div>
	<div class="login-button">Login</div>
	<div class="subscribe-button">Subscribe</div>
</div>

<div class="content"></div>

<div id="main">