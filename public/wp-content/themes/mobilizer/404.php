<?php	
	$page = get_page_by_path('page-not-found');
	$url = get_permalink($page->ID);
	wp_redirect($url, 302); 
?>