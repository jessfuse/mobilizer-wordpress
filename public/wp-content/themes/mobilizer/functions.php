<?php
remove_filter('template_redirect','redirect_canonical');

$states = array(
   'AL'=>'Alabama','AK'=>'Alaska','AZ'=>'Arizona','AR'=>'Arkansas','CA'=>'California','CO'=>'Colorado','CT'=>'Connecticut','DE'=>'Delaware','DC'=>'District of Columbia','FL'=>'Florida','GA'=>'Georgia','HI'=>'Hawaii','ID'=>'Idaho','IL'=>'Illinois','IN'=>'Indiana','IA'=>'Iowa','KS'=>'Kansas','KY'=>'Kentucky','LA'=>'Louisiana','ME'=>'Maine','MD'=>'Maryland','MA'=>'Massachusetts','MI'=>'Michigan','MN'=>'Minnesota','MS'=>'Mississippi','MO'=>'Missouri','MT'=>'Montana','NE'=>'Nebraska','NV'=>'Nevada','NH'=>'New Hampshire','NJ'=>'New Jersey','NM'=>'New Mexico','NY'=>'New York','NC'=>'North Carolina','ND'=>'North Dakota','OH'=>'Ohio','OK'=>'Oklahoma','OR'=>'Oregon','PA'=>'Pennsylvania','RI'=>'Rhode Island','SC'=>'South Carolina','SD'=>'South Dakota','TN'=>'Tennessee','TX'=>'Texas','UT'=>'Utah','VT'=>'Vermont','VA'=>'Virginia','WA'=>'Washington','WV'=>'West Virginia','WI'=>'Wisconsin','WY'=>'Wyoming');

/**
 * Include the composer autoloader
 * 
 */
require __DIR__ . '/vendor/autoload.php';

/**
 * Enable the Whoops error handler and PHP console if we're debugging
 */
if(WP_DEBUG)
{
	$whoops = new \Whoops\Run;
	$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
	$whoops->register();

	\PhpConsole\Connector::setPostponeStorage(new \PhpConsole\Storage\File('/tmp/pc.data'));
	$handler = \PhpConsole\Handler::getInstance();
	$handler->setHandleErrors(false);
	$handler->setHandleExceptions(false);
	$handler->start();
}

/**
 * Disable AutoP
 */
remove_filter( 'the_content', 'wpautop' );

/**
 * Auto-load all custom post type classes
 */
/* SBX\CPT::load(__DIR__ . '/classes/sbx/cpt', 'SBX\\CPT'); */

/**
 * Auto-load all shortcodes
 * 
 */
/* SBX\Shortcode::load(__DIR__ . '/classes/sbx/shortcode', 'SBX\\Shortcode'); */


/**
 * Enqueue styles and scripts
 *
 * Properly enqueues styles and scripts per WordPress Standards
 * 
 * @return void
 */
function lp_enqueue_scripts () {
    // wp_deregister_script('jquery');
    // wp_deregister_script('jquery');
    // wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), '1.11.0', true);

    wp_enqueue_script('lawpay-lib', get_template_directory_uri() . '/client/dist/scripts/lib.min.js', array(), '1.0.0', true);
    wp_enqueue_script('lawpay', get_template_directory_uri() . '/client/dist/scripts/app.min.js', array('lawpay-lib'), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'lp_enqueue_scripts');


function lp_dequeue_scripts () {

    wp_dequeue_style('kebo-twitter-plugin');
}

add_action('wp_footer', 'lp_dequeue_scripts');



/**
 * Create customization options
 * 
 */

function lawpay_customize_register( $wp_customize ) {

	class Customize_Textarea_Control extends WP_Customize_Control {
	    public $type = 'textarea';
	    public function render_content() {
	        ?>
	        <label>
	        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
	        <textarea rows="3" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
	        </label>
	        <?php
	    }
	}

   $wp_customize->add_section( 'footer_settings', array(
        'title'          => 'Footer',
        'priority'       => 35,
    ) );
 
    $wp_customize->add_setting( 'footer_text', array(
        'default'        => '',
    ) );
 
    $wp_customize->add_control(new Customize_Textarea_Control($wp_customize, 'footer_text', array(
        'label'   => 'About Text',
        'section' => 'footer_settings',
        'type'    => 'text',
    ) ));



    $wp_customize->add_setting( 'sales_number', array(
        'default'        => '',
    ) );	

    $wp_customize->add_control( 'sales_number', array(
        'label'   => 'Sales Phone Number',
        'section' => 'footer_settings',
        'type'    => 'text',
    ) );


    $wp_customize->add_setting( 'support_number', array(
        'default'        => '',
    ) );

    $wp_customize->add_control( 'support_number', array(
        'label'   => 'Support Phone Number',
        'section' => 'footer_settings',
        'type'    => 'text',
    ) );

    $wp_customize->add_setting( 'fax_number', array(
        'default'        => '',
    ) );

    $wp_customize->add_control( 'fax_number', array(
        'label'   => 'Fax Number',
        'section' => 'footer_settings',
        'type'    => 'text',
    ) );



    $wp_customize->add_setting( 'facebook', array(
        'default'        => '#',
    ) );

    $wp_customize->add_control( 'facebook', array(
        'label'   => 'Facebook URL',
        'section' => 'footer_settings',
        'type'    => 'text',
    ) );


    $wp_customize->add_setting( 'twitter', array(
        'default'        => '#',
    ) );

    $wp_customize->add_control( 'twitter', array(
        'label'   => 'Twitter URL',
        'section' => 'footer_settings',
        'type'    => 'text',
    ) );


    $wp_customize->add_setting( 'linkedin', array(
        'default'        => '#',
    ) );

    $wp_customize->add_control( 'linkedin', array(
        'label'   => 'LinkedIn URL',
        'section' => 'footer_settings',
        'type'    => 'text',
    ) );
 
}

add_action( 'customize_register', 'lawpay_customize_register' );


/**
 * Register menus
 * 
 */
register_nav_menus(array(
	'primary' => 'Main Menu'
));


/**
 * Add Page Slug to Body Class
 * 
 */
function add_slug_body_class( $classes ) {
  global $post;
  if ( isset( $post ) ) {
    $classes[] = $post->post_type . '-' . $post->post_name;

    wp_localize_script('lawpay-lib', 'page', array(
        'name' => $post->post_name
    ));
  }
  return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


/**
 * Add Thumbnail Support
 * 
 */
add_theme_support('post-thumbnails');


/**
 * Tell gravity forms to render at the bottom of the page
 * 
 */
add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
        return 'document.addEventListener("DOMContentLoaded",function(){ ';
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
    return ' });';
}
add_filter('gform_init_scripts_footer', '__return_true');




function get_endorsements ($arg) {

    $state = $_GET['state'];
    echo  do_shortcode('[endorsements-results state="' . $state . '"]'); die();
}

add_action( 'wp_ajax_get_endorsements', 'get_endorsements' );
add_action( 'wp_ajax_nopriv_get_endorsements', 'get_endorsements' );

function open_account () {

    // header("HTTP/1.0 404 Not Found");
    echo '{"errors": { "business_name": ["cant be blank"]}, "messages":["First name cant be blank"]}';
    die();

}

add_action( 'wp_ajax_open_account', 'open_account' );
add_action( 'wp_ajax_nopriv_open_account', 'open_account' );

/* imports endorsements from a spreadsheet */
// function import_endorsements () {
//     $end = new LP\util\Endorsements(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
//     print_r($end->import_endorsements($_GET['path'])); die();
// }
// add_action( 'wp_ajax_import_endorsements', 'import_endorsements' );

?>