<?php get_header(); ?>


<div class="page">
	<h1><?php echo $post->post_title; ?></h1>	
	
	<?php
		if(have_posts())
		{
			while(have_posts())
			{
				the_post();
				the_content();
			}
		}
	?>
</div>
<!-- /.page -->

<?php get_footer(); ?>