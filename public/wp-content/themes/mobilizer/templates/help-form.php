<?php
/*
Template Name: helpform
*/

?>

<?php get_header('secondary'); ?>

<div class="help-wrapper">
	<div class="supportCTA">
		<div class="form-wrapper">
			<h1 class="main-header">Help</h1>
			<p class="subheader">Have an issue?  Tell us what it is and we'll help you out.</p>
			<form class="issue-info">
				<input class="name required" type="text" placeholder="Full name">
				<input class="email required" type="text" placeholder="Email">
				<input class="title required" type="text" placeholder="Issue title">
				<input class="description required" type="text" placeholder="Issue description">
				<div class="btn-wrapper"><button class="submit">Submit</button></div>
			</form>
		</div>
	</div>
</div>

<?php get_footer(); ?>