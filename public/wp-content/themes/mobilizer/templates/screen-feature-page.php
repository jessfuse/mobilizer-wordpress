<?php
/*
Template Name: screenfeaturepage
*/

?>

<?php get_header(); ?>

<div class="on-screen-container">

	<div class="header-url">
		<input class="url-input" id="enteredUrlTop" placeholder="www."/>
		<div class="go-button">Go</div>
	</div>

	<div class="mainCTA">
		<div class="main-header-wrapper">
			<h1>On Screen.</h1>
			<h2>Preview on screen.  Easy image export.</h2>
		</div>
		<div class="video"><img src="<?php echo get_template_directory_uri() . '/client/dist/images/video-img.png'; ?>"></div>
	</div>
	<div class="section-3-wrapper">
		<div class="left-wrapper">
			<div class="devices"><img src="<?php echo get_template_directory_uri() . '/client/src/images/on-screen-illustration1.png'; ?>"></div>
			<div class="text">
				<h1>Mobile Previews</h1>
				<h2>Mobilizer’s simple interface gives designers and presenters the tools to create impressive mobile previews that will take your presentations to the next level.  Enter a URL or drag and drop your .png, .psd or .html file.</h2>
			</div>
		</div>
		<div class="right-wrapper">
			<div class="report"><img src="<?php echo get_template_directory_uri() . '/client/src/images/on-screen-illustration2.png'; ?>"></div>
			<div class="text">
				<h1>Easy Export</h1>
				<h2>Export device images with transparent backgrounds – ideal for simple, contextual views of your design and perfect for enhancing your portfolios, pitches and presentations.</h2>
			</div>
		</div>
	</div>
	<div class="section-4-wrapper">
		<h1 class="final-cta">Save time and money when you create your images with Mobilizer.</h1>
		<div class="subscribe-wrapper">
			<button class="subscription-btn">see subscription plans</button>
		</div>
	</div>

</div>
<?php get_footer(); ?>
