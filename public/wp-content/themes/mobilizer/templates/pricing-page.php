<?php
/*
Template Name: pricingpage
*/

?>

<?php get_header(); ?>

<div class="pricing-page-wrapper">
	<div class="mainCTA">
		<div class="main-header-wrapper">
			<h1>Subscribe and Start Mobilizing</h1>
		</div>
	</div>

	<div class="plan-wrapper">
		<div class="plan plan-one">
			<div class="price">
				<h4 class="mode">On Screen</h4>
				<h1>$99</h1>
				<h4 class="unlimited">Unlimited</h4>
			</div>
			<div class="description cta">
				<button class="subscribe">Join</button>
				<p>If you are a designer or presenting information, this option is for you.</p>
				<ul>
					<li>High resolution</li>
					<li>No license restrictions</li>
					<li>Export transparent png images</li>
				</ul>
			</div>
		</div>

		<div class="plan plan-two">
			<div class="price">
				<h4 class="mode">On The Metal</h4>
				<h1>$199</h1>
				<h4 class="unlimited">Unlimited</h4>
			</div>
			<div class="description cta">
				<button class="subscribe">Join</button>
				<p>If you are a developer or a quality assurance tester, this option is for you.</p>
				<ul>
					<li>Current iOS and Android devices with the largest market share</li>
					<li>Concurrent device testing</li>
					<li>Generate reports with metadata and rendering times</li>
				</ul>
			</div>
		</div>

		<div class="plan plan-three">
			<div class="price">
				<h4 class="mode">Plus</h4>
				<h1>$249</h1>
				<h4 class="unlimited">Unlimited</h4>
			</div>
			<div class="description cta">
				<button class="subscribe">Join</button>
				<p>If you are an agency or company, this option is for you.</p>
			</div>
		</div>

		<div class="plan plan-four">
			<div class="price">
				<h4 class="mode">Enterprise</h4>
				<h1>Custom</h1>
				<h4 class="unlimited">Let us create a plan for you & your team</h4>
			</div>
			<div class="description cta">
				<button class="subscribe">Join</button>
				<p>If you are looking for a custom solution with dedicated devices, this option is for you.</p>
			</div>
		</div>
	</div>	
</div>

<?php get_footer(); ?>