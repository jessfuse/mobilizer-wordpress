<?php
/*
Template Name: landingpage
*/

?>

<?php get_header(); ?>

<div class="landing-container">

	<div class="hero-section">
		<div class="hero-positioning">
			<div class="monitor-img"><img src="<?php echo get_template_directory_uri() . '/client/dist/images/hero-monitor.png'; ?>"></div>
			<div class="empty-cell"></div>
			<div class="hero-info">
				<h1 class="main-header">Mobilize your workflow.</h1>
				<p class="main-subhead">Preview mobile websites from the comfort of your desktop.</p>
				<div class="url-relative-wrapper">
					<div class="header-url">
						<input class="url-input" id="enteredUrlTop" placeholder="www."/>
						<div class="go-button">Go</div>
					</div>
				</div>
			</div>
		</div>

	<!--		<form method="get" action="http://localhost:8000/#/" onsubmit="return configUrlTop()" /></form>-->
		<div class="dark-banner"></div>
	</div>

	<div class="mode-section">
		<h1 class="main-header">Two ways to use Mobilizer!</h1>
		<div class="on-screen">
			<h2 class="mode">On Screen</h2>
			<ul class="mode-details">
				<li>Preview sites and designs in mobile context</li>
				<li>Simulate a wide variety of phones and tablets</li>
				<li>Export presentation-ready image captures</li>
			</ul>
			<button class="mode-cta">Learn More</button>
		</div>
		<div class="on-the-metal">
			<h2 class="mode">On The Metal</h2>
			<ul class="mode-details">
				<li>Download screen shots from real devices</li>
				<li>See webpages exactly as your users do</li>
				<li>Test on the latest devices</li>
			</ul>
			<button class="mode-cta">Learn More</button>
		</div>
	</div>

	<div class="video-section">
		<div class="video-text">
			<h2 class="video-header">Speed up Your Design, Development and Presentation.</h2>
			<p>Eliminate the need to buy and update expensive tablets and phones.  Test more devices faster, and get the job done sooner.  Ensure sites run smoothly, optimizing potential traffic.</p>
		</div>
		<div class="video"><img src="<?php echo get_template_directory_uri() . '/client/dist/images/video-img.png'; ?>"></div>
	</div>

	<div class="brands-section">
		<div class="mobilizer-header">
			<h1>Testing on Mobile is Big Business.</h1>
			<p>Teams and individuals across the globe use Mobilizer to speed up their design, development and presentation process.</p>
		</div>
		<div class="logo-container">
			<div class="logos"><img src="<?php echo get_template_directory_uri() . '/client/dist/images/logos.png'; ?>"></div>
		</div>
	</div>

</div>


<?php get_footer(); ?>

