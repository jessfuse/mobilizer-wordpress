<?php
/*
Template Name: metalfeaturepage
*/

?>

<?php get_header(); ?>

<div class="on-the-metal-container">

	<div class="header-url">
		<input class="url-input" id="enteredUrlTop" placeholder="www."/>
		<div class="go-button">Go</div>
	</div>

	<div class="mainCTA">
		<div class="main-header-wrapper">
			<h1>On The Metal.</h1>
			<h2>See the real deal.  Test on devices.</h2>
		</div>
		<div class="video"><img src="<?php echo get_template_directory_uri() . '/client/dist/images/video-img.png'; ?>"></div>
	</div>

	<div class="section-2-wrapper">
		<div class="device-text">
			<h1>Modern Devices.</h1>
			<h2>Maintaining a mobile testing lab is expensive and time consuming.  We offer the most current iOS, Andriod and Fire phones and tablets to ensure that devices with the largest market share are included in the Mobilizer testing lab.</h2>
		</div>
		<div class="device-row"><img src="<?php echo get_template_directory_uri() . '/client/dist/images/phones-metal.png'; ?>"></div>
	</div>

	<div class="section-3-wrapper">
		<div class="left-wrapper">
			<div class="metal-testing"><img src="<?php echo get_template_directory_uri() . '/client/src/images/on-the-metal-illustration1.png'; ?>"></div>
			<div class="text">
				<h1>Pixel Precision</h1>
				<h2>Developers and QA pros will appreciate that Moblizer’s simple interface delivers a preview of a site exactly as it will appear on the device.   Compare site rendering from one device to another.  Then generate full page image captures and reports with device metadata and rendering times.</h2>
			</div>
		</div>
		<div class="right-wrapper">
			<div class="metal-report"><img src="<?php echo get_template_directory_uri() . '/client/src/images/on-the-metal-illustration2.png'; ?>"></div>
			<div class="text">
				<h1>Concurrent Testing</h1>
				<h2>Are your breakpoints breaking?  Compare the latest phones and tablets at once and dramatically reduce testing time compared to on-site mobile testing labs.</h2>
			</div>
		</div>
	</div>
	<div class="section-4-wrapper">
		<h1 class="final-cta">Save Time and Money by using our Testing Lab.</h1>
		<div class="subscribe-wrapper">
			<button class="subscription-btn">see subscription plans</button>
		</div>
	</div>

</div>
<?php get_footer(); ?>




