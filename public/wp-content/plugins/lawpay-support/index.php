<?php
/*
Plugin Name: LawPay Support
Plugin URI: http://www.lawpay.com
Description: Accordion based WordPress Support. 
Version: 1.0.0
Author: Daniel TIjerina
Author URI: http://www.springbox.com
License: GPL2
http://www.gnu.org/licenses/gpl-2.0.html
*/

//Custom FAQ Post Type 
function lawpay_support() {

    $args = array(
        'labels'        => array(
            'name'               => _x( 'Support', 'post type general name' ),
            'singular_name'      => _x( 'Support', 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'book' ),
            'add_new_item'       => __( 'Add New Support' ),
            'edit_item'          => __( 'Edit Support' ),
            'new_item'           => __( 'New Support Items' ),
            'all_items'          => __( 'All Support Items' ),
            'view_item'          => __( 'View Support Item' ),
            'search_items'       => __( 'Search Support' ),
            'not_found'          => __( 'No Support Items found' ),
            'not_found_in_trash' => __( 'No Support Items found in the Trash' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'Support'
        ),
        'description'   => 'Holds Support specific data',
        'public'        => true,
        'show_ui'       => true,
        'show_in_menu'  => true,
        'query_var'     => true,
        'rewrite'       => true,
        'capability_type'=> 'post',
        'has_archive'   => true,
        'hierarchical'  => false,
        'menu_position' => 5,
        'supports'      => array( 'title', 'editor')
    );

    register_post_type( 'support', $args ); 


    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Support Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Support Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Support Categories' ),
        'all_items'         => __( 'All Support Categories' ),
        'parent_item'       => __( 'Parent Support Category' ),
        'parent_item_colon' => __( 'Parent Support Category:' ),
        'edit_item'         => __( 'Edit Support Category' ),
        'update_item'       => __( 'Update Support Category' ),
        'add_new_item'      => __( 'Add New Support Category' ),
        'new_item_name'     => __( 'New Support Category Name' ),
        'menu_name'         => __( 'Support Category' ),
    );

    register_taxonomy('support_cat',array('support'), array(
        'hierarchical' => true,
        'labels'       => $labels,
        'show_ui'      => true,
        'query_var'    => true,
        'rewrite'      => array( 'slug' => 'support_cat' ),
    ));
}

add_action( 'init', 'lawpay_support' );

function lawpay_support_scripts(){
     if(!is_admin()){
        
        wp_register_script('lawpay-custom-js', plugins_url('/accordion.js', __FILE__ ), array(),false, true);
        wp_enqueue_script('lawpay-custom-js');
    }   
}



function lawpay_support_shortcode($atts) { 
// Registering the scripts and style
    lawpay_support_scripts();

    global $post;

    extract(shortcode_atts(array(
        'id' => ''
    ), $atts));

    ob_start();
    
    $tax_terms = get_terms( 'support_cat', array(
        'orderby'=>'name', 
        'order' => 'ASC'
    ));
    if ($tax_terms) {

        ?><div class="container faqs"> <?php
        
        foreach ($tax_terms  as $tax_term) {

            $args = array(
                'post_type'         => 'support',
                'support_cat'           => $tax_term->slug,
                'post_status'       => 'publish',
                'posts_per_page'    => -1
            );

            $query = new WP_Query($args);

            if( $query->have_posts() ) : ?>
                
                <h3 class="faq-category"  data-scroll-track="<?php echo sanitize_title($tax_term->name);?>"><?php echo $tax_term->name;?></h3>
                <div class="faq-group">

                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    
                    <div class="faq-item">
                        <h4 class="faq-title">
                            <a href="javascript:;" class="faq-link" onclick="ga('send', 'event', 'customer-support', 'click', '<?php echo $post->post_name; ?>')"><?php echo get_the_title($post);?></a>
                        </h4>
                        <div class="faq-content">
                            <div class="faq-content-wrapper"><?php echo apply_filters('the_content', wpautop(get_the_content($post))); ?></div>
                        </div>
                    </div>

                <?php endwhile; // end of loop ?>

                </div>

            <?php endif; // if have_posts()
            
            wp_reset_query();
            wp_reset_postdata();
        }

        ?> </div> <?php

        
    }

    $res = ob_get_contents();
    ob_end_clean();

    return $res;

}
add_shortcode('support', 'lawpay_support_shortcode');

?>