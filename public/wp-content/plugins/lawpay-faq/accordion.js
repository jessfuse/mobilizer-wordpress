(function ($) {
	$('.faq-item').each(function(i, el){
		
		var $el = $(el);
		var $link = $el.find('.faq-link');
		var $content = $el.find('.faq-content');
		var active = false;

		$content.slideUp();

		$el.find('.faq-link').click(function () {

			if ($el.hasClass('active')) {
				$content.slideUp();
				$el.removeClass('active');
			} else {
				$content.slideDown();
				$el.addClass('active');
			}
		});
	});
}(jQuery));
