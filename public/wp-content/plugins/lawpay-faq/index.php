<?php
/*
Plugin Name: lawpay FAQ
Plugin URI: http://www.lawpay.com
Description: Accordion based WordPress FAQ. 
Version: 1.0.0
Author: Daniel TIjerina
Author URI: http://www.springbox.com
License: GPL2
http://www.gnu.org/licenses/gpl-2.0.html
*/

//Custom FAQ Post Type 
function lawpay_faq() {

    $args = array(
        'labels'        => array(
            'name'               => _x( 'FAQ', 'post type general name' ),
            'singular_name'      => _x( 'FAQ', 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'book' ),
            'add_new_item'       => __( 'Add New FAQ' ),
            'edit_item'          => __( 'Edit FAQ' ),
            'new_item'           => __( 'New FAQ Items' ),
            'all_items'          => __( 'All FAQ\'s' ),
            'view_item'          => __( 'View FAQ' ),
            'search_items'       => __( 'Search FAQ' ),
            'not_found'          => __( 'No FAQ Items found' ),
            'not_found_in_trash' => __( 'No FAQ Items found in the Trash' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'FAQ'
        ),
        'description'   => 'Holds FAQ specific data',
        'public'        => true,
        'show_ui'       => true,
        'menu_icon'     => 'dashicons-editor-help',
        'show_in_menu'  => true,
        'query_var'     => true,
        'rewrite'       => true,
        'capability_type'=> 'post',
        'has_archive'   => true,
        'hierarchical'  => false,
        'menu_position' => 5,
        'supports'      => array( 'title', 'editor')
    );

    register_post_type( 'faqs', $args ); 


    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'FAQ Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'FAQ Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search FAQ Categories' ),
        'all_items'         => __( 'All FAQ Category' ),
        'parent_item'       => __( 'Parent FAQ Category' ),
        'parent_item_colon' => __( 'Parent FAQ Category:' ),
        'edit_item'         => __( 'Edit FAQ Category' ),
        'update_item'       => __( 'Update FAQ Category' ),
        'add_new_item'      => __( 'Add New FAQ Category' ),
        'new_item_name'     => __( 'New FAQ Category Name' ),
        'menu_name'         => __( 'FAQ Category' ),
    );

    register_taxonomy('faq_cat',array('faqs'), array(
        'hierarchical' => true,
        'labels'       => $labels,
        'show_ui'      => true,
        'query_var'    => true,
        'rewrite'      => array( 'slug' => 'faq_cat' ),
    ));
}

add_action( 'init', 'lawpay_faq' );

function lawpay_scripts(){
     if(!is_admin()){

        wp_register_script('lawpay-custom-js', plugins_url('/accordion.js', __FILE__ ), array(),false, true);
        wp_enqueue_script('lawpay-custom-js');
    }   
}



function lawpay_accordion_shortcode($atts) { 
// Registering the scripts and style
    lawpay_scripts();

    global $post;

    extract(shortcode_atts(array(
        'id' => ''
    ), $atts));

    ob_start();
    
    $tax_terms = get_terms( 'faq_cat', array(
        'orderby'=>'name', 
        'order' => 'ASC'
    ));
    if ($tax_terms) {

        ?><div class="container faqs"  data-scroll-track="faqs"> <?php
        
        foreach ($tax_terms  as $tax_term) {

            $args = array(
                'post_type'         => 'faqs',
                'faq_cat'           => $tax_term->slug,
                'post_status'       => 'publish',
                'posts_per_page'    => -1
            );

            $query = new WP_Query($args);
            // add_filter( 'the_content', 'wpautop' );

            if( $query->have_posts() ) : ?>
                
                <h3 class="faq-category"><?php echo $tax_term->name;?></h3>
                <div class="faq-group">

                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    
                    <div class="faq-item">
                        <h4 class="faq-title">
                            <a href="javascript:;" class="faq-link" onclick="ga('send', 'event', 'faqs', 'click', '<?php echo $post->post_name; ?>')"><?php echo get_the_title($post);?></a>
                        </h4>
                        <div class="faq-content">
                            <div class="faq-content-wrapper"><?php echo apply_filters('the_content', wpautop(get_the_content($post))); ?></div>
                        </div>
                    </div>

                <?php endwhile; // end of loop ?>

                </div>

            <?php endif; // if have_posts()<?


            
            wp_reset_query();
            wp_reset_postdata();
            // remove_filter( 'the_content', 'wpautop' );
        }

        ?> </div> <?php

        
    }

    $res = ob_get_contents();
    ob_end_clean();

    return $res;

}
add_shortcode('faqs', 'lawpay_accordion_shortcode');